package ru.smochalkin.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.smochalkin.tm.event.ConsoleEvent;
import ru.smochalkin.tm.listener.AbstractTaskListener;
import ru.smochalkin.tm.endpoint.TaskDto;
import ru.smochalkin.tm.enumerated.Sort;
import ru.smochalkin.tm.exception.system.AccessDeniedException;
import ru.smochalkin.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

@Component
public final class TaskShowListListener extends AbstractTaskListener {

    @Override
    @NotNull
    public String name() {
        return "task-list";
    }

    @Override
    @NotNull
    public String description() {
        return "Display list of tasks.";
    }

    @Override
    @EventListener(condition = "@taskShowListListener.name() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        if (sessionService.getSession() == null) throw new AccessDeniedException();
        System.out.println("Enter sort option from list:");
        System.out.println(Arrays.toString(Sort.values()));
        @NotNull final List<TaskDto> tasks;
        String sortName = TerminalUtil.nextLine();
        if (sortName.isEmpty()) {
            tasks = taskEndpoint.findTaskAll(sessionService.getSession());
        } else {
            tasks = taskEndpoint.findTaskAllSorted(sessionService.getSession(), sortName);
        }
        int index = 1;
        for (TaskDto task : tasks) {
            System.out.println(index++ + ". " + task.getId() + "|" + task.getName() + "|" + task.getStatus());
        }
    }

}
