package ru.smochalkin.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.smochalkin.tm.event.ConsoleEvent;
import ru.smochalkin.tm.listener.AbstractListener;
import ru.smochalkin.tm.endpoint.Result;
import ru.smochalkin.tm.exception.system.AccessDeniedException;
import ru.smochalkin.tm.util.TerminalUtil;

@Component
public class UserChangePassListener extends AbstractListener {

    @Override
    @NotNull
    public String name() {
        return "user-change-password";
    }

    @Override
    @NotNull
    public String description() {
        return "Change user password.";
    }

    @Override
    @EventListener(condition = "@userChangePassListener.name() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        if (sessionService.getSession() == null) throw new AccessDeniedException();
        System.out.println("Enter new password:");
        @NotNull final String password = TerminalUtil.nextLine();
        @NotNull final Result result =  userEndpoint.userSetPassword(sessionService.getSession(), password);
        printResult(result);
    }

}

