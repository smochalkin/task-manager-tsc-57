package ru.smochalkin.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import ru.smochalkin.tm.event.ConsoleEvent;
import ru.smochalkin.tm.listener.AbstractListener;
import ru.smochalkin.tm.endpoint.Result;
import ru.smochalkin.tm.exception.system.AccessDeniedException;
import ru.smochalkin.tm.util.TerminalUtil;

public class UserUpdateListener extends AbstractListener {

    @Override
    @NotNull
    public String name() {
        return "user-update";
    }

    @Override
    @NotNull
    public String description() {
        return "Update a user.";
    }

    @Override
    @EventListener(condition = "@userUpdateListener.name() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        if (sessionService.getSession() == null) throw new AccessDeniedException();
        System.out.println("Enter first name:");
        @NotNull final String firstName = TerminalUtil.nextLine();
        System.out.println("Enter last name:");
        @NotNull final String lastName = TerminalUtil.nextLine();
        System.out.println("Enter middle name:");
        @NotNull final String middleName = TerminalUtil.nextLine();
        @NotNull final Result result = userEndpoint
                .userUpdate(sessionService.getSession(), firstName, lastName, middleName);
        printResult(result);
    }

}
