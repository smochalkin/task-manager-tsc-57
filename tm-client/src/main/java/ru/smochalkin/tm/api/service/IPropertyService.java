package ru.smochalkin.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface IPropertyService {

    @NotNull
    Integer getPasswordIteration();

    @NotNull
    String getPasswordSecret();

}
