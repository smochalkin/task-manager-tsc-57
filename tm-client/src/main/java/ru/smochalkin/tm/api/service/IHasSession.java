package ru.smochalkin.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.endpoint.SessionDto;

public interface IHasSession {

    @Nullable
    SessionDto getSession();

    void setSession(@Nullable final SessionDto status);

}
