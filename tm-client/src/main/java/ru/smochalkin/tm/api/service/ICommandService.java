package ru.smochalkin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.listener.AbstractListener;
import ru.smochalkin.tm.listener.AbstractSystemListener;

import java.util.Collection;
import java.util.List;

public interface ICommandService {

    @NotNull
    Collection<AbstractListener> getCommands();

    @NotNull
    Collection<AbstractSystemListener> getArguments();

    @NotNull
    List<String> getCommandNames();

    @NotNull
    List<String> getCommandArgs();

    @Nullable
    AbstractListener getCommandByName(@NotNull String name);

    @Nullable
    AbstractListener getCommandByArg(@NotNull String arg);

    void add(@Nullable AbstractListener command);

}
