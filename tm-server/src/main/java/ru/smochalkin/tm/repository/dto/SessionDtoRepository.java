package ru.smochalkin.tm.repository.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.smochalkin.tm.api.repository.dto.ISessionDtoRepository;
import ru.smochalkin.tm.dto.SessionDto;

import javax.persistence.EntityManager;
import java.util.List;

@Getter
@Repository
@Scope("prototype")
public class SessionDtoRepository extends AbstractDtoRepository<SessionDto> implements ISessionDtoRepository {

    @Override
    public void clear() {
        entityManager
                .createQuery("DELETE FROM SessionDto e")
                .executeUpdate();
    }

    @NotNull
    @Override
    public List<SessionDto> findAll() {
        return entityManager.createQuery("SELECT e FROM SessionDto e", SessionDto.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .getResultList();
    }

    @Override
    @NotNull
    public List<SessionDto> findAllByUserId(@NotNull final String userId) {
        return entityManager.createQuery("SELECT e FROM SessionDto e WHERE e.userId = :userId", SessionDto.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    @Nullable
    public SessionDto findById(@Nullable final String id) {
        return entityManager.find(SessionDto.class, id);
    }

    @Override
    public void removeById(@Nullable final String id) {
        SessionDto reference = entityManager.getReference(SessionDto.class, id);
        entityManager.remove(reference);
    }

    @Override
    public void removeByUserId(@NotNull final String userId) {
        entityManager
                .createQuery("DELETE FROM SessionDto e WHERE e.userId = :userId")
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    public int getCount() {
        return entityManager
                .createQuery("SELECT COUNT(e) FROM SessionDto e", Long.class)
                .getSingleResult()
                .intValue();
    }

}
